#include <string>
#include <iostream>
#include <rciDaemon/rciDeviceBase.h>

#ifndef _FILEDEVICE_H_
#define _FILEDEVICE_H_

namespace Rci
{
/** This class defines the screen sizes
 */
class FileDevice : public RciDeviceBase
{
protected:
    std::string _lastRequestedPage;

    std::ostream* _ptrTargetStream;
    
public:
    FileDevice(const std::string &inPath);
    FileDevice(const std::string &inPath, const char* inName);
    virtual ~FileDevice();
    
    virtual int init(const std::string &inConfigPath);
    virtual int echoPage(const std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName);
    virtual void setAnswer( std::string const&, Rci::CmdAnswer const&, Rci::DeviceSpecificData*) {;}

    virtual int translate( RcCode &outCmd){return -1;} ///< gets the command received from the input
};

}

#endif
