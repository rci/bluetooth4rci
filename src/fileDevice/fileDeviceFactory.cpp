#include <fstream>
#include "fileDevice.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::FileDevice *maker(const std::string &inPath)   ///< Factory
{
    return new Rci::FileDevice(inPath);
}

void eraser(Rci::RciDeviceBase *in_Ptr)   ///< Factory
{
    delete in_Ptr;
}
#ifdef __cplusplus
}
#endif

