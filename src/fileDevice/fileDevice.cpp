#include <fstream>
#include "fileDevice.h"

Rci::FileDevice::FileDevice(const std::string &inPath) :  RciDeviceBase(inPath, "fileDevice"),
      _ptrTargetStream(NULL)
{
//  init(inPath);
}

Rci::FileDevice::FileDevice(const std::string &inPath, const char* inName) :
      RciDeviceBase(inPath, inName), _ptrTargetStream(NULL)
{
//  init(inPath);
}

Rci::FileDevice::~FileDevice()
{
  if( _ptrTargetStream != NULL )
    delete _ptrTargetStream;
}

int Rci::FileDevice::init(const std::string &inConfigPath)
{
  if( _ptrTargetStream == NULL)
    _ptrTargetStream = new std::ofstream(inConfigPath.c_str());
  return 0;
}

int Rci::FileDevice::echoPage(const std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName)
{
  if( _ptrTargetStream != NULL )
  {
    bool force = false;
    if( _lastRequestedPage != inOutpageName )
    {
      force = true;
      _lastRequestedPage = inOutpageName;
    }

    for( std::list<RciOutpage>::const_iterator itData = inDataToDisplay.begin();
         itData != inDataToDisplay.end(); ++itData )
    {
      if( itData->constGetChanged() || force )
        *_ptrTargetStream << "value: " << itData->getText() << std::endl;
    }
  }
}

