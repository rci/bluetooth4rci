#include "phoneDevice.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::PhoneDevice *maker(const std::string &inPath)   ///< Factory
{
    return new Rci::PhoneDevice(inPath);
}

void eraser(Rci::RciDeviceBase *in_Ptr)   ///< Factory
{
    delete in_Ptr;
}
#ifdef __cplusplus
}
#endif

