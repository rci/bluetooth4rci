#include <fstream>
#include <sstream>
#include <confuse.h>
#include <errno.h>

extern "C" {
  #include "myBluetooth.h"
}

#include "phoneDevice.h"

Rci::PhoneDevice::PhoneDevice(const std::string &inPath) :  Rci::FileDevice(inPath, "PhoneDevice"),
    _svc_uuid_int({ 0xC4, 0x99, 0x94, 0x90, 0x84, 0xD8, 0x11, 0xE1,
                    0x98, 0x1A, 0x00, 0x0D, 0xF0, 0x59, 0x72, 0x3E })
{

  init(inPath);
}

Rci::PhoneDevice::~PhoneDevice()
{
}


int Rci::PhoneDevice::init(const std::string &inConfigPath)
{
  int ans = 0;
  //bluetooth tests
  // creates the device uuid.
  uuid_t svc_uuid;
  sdp_uuid128_create( &svc_uuid, &_svc_uuid_int );  

  std::ostringstream ssMessage;

  advertise(svc_uuid, "test", "test", "test");

  return ans;
}

/** @param[in] inMsg : message to send
 *  @param[in] field : number of the field
 *  @param[out] outMsg : returned message
 *  @return message
 */
void Rci::PhoneDevice::makeMessageDisplay( const std::string& inMsg, unsigned char field, MessageData& outMsg)
{
  outMsg.resize(inMsg.size() + 4 + 1 + 1 + 1); // message + size + command + field + termination
  
  for( int i = 0; i < 4; ++i )
    outMsg[3 - i] = (outMsg.size() >> (i * 8));

  outMsg[4] = (char) Display;
  outMsg[5] = field;
  
  inMsg.copy( &outMsg[6], inMsg.size());
  outMsg.back() = '\n';
}

/** @param[in] inMsg : message to send
 *  @param[in] field : number of the field
 *  @param[out] outMsg : returned message
 *  @return message
 */
void Rci::PhoneDevice::makeMessageAction( const std::string& inMsg, unsigned char field, MessageData& outMsg )
{
  makeMessageDisplay( inMsg, field, outMsg);
  outMsg[4] = (char) Action;
}

/** @param[in] inMsg : message to send
 *  @param[out] outSize : size of message
 *  @return message
 */
void Rci::PhoneDevice::makeMessageAnswerAction( const std::string& inMsg, MessageData& outMsg )
{
  outMsg.resize( inMsg.size() + 1 + sizeof(PacketSize) + 1 );
  
  for( int i = 0; i < 4; ++i )
    outMsg[3 - i] = ( outMsg.size() >> (i * 8) ); // message size
  
  outMsg[4] = (char) AnswerAction;
  
  inMsg.copy( &outMsg[5], inMsg.size() );
  outMsg.back() = '\n';
}

/** @param[in] inMsg : message to send
 *  @param[in] index : number of the answer
 *  @param[in] numberOfAnswers : total number of answers
 *  @param[out] outMsg : returned message
 */
void Rci::PhoneDevice::makeMessageAnswer( const std::string& inMsg, unsigned char index, unsigned char numberOfAnswers, MessageData& outMsg )
{
  outMsg.resize( inMsg.size() + 1 + sizeof(PacketSize) * 3 + 1 );
    
  for( int i = 0; i < 4; ++i )
  {
    outMsg[3 - i] = (  outMsg.size() >> (i * 8)); // message size
    outMsg[8 - i] = (          index >> (i * 8)); // index of the answer
    outMsg[12- i] = (numberOfAnswers >> (i * 8)); // total number of the answer
  }
  
  outMsg[4] = (char) Answers;
  
  inMsg.copy( &outMsg[13], inMsg.size() );
  outMsg.back() = '\n';
}

/** @param[out] outSize : size of message
 *  @return message
 */
void Rci::PhoneDevice::makeMessageClear( MessageData& outMsg )
{
  outMsg.resize( 1 + 4 + 1 );
  
  outMsg[0] = 0x00;
  outMsg[1] = 0x00;
  outMsg[2] = 0x00;
  outMsg[3] = 0x06;
  outMsg[4] = (char) Clear;
  outMsg[5] = '\n';
}

void Rci::PhoneDevice::onConnection( BluetoothConnection_ptr inDevice )
{
  MessageData value, action;
  std::ostringstream ssMessage;

  unsigned char field = 0;
  for( std::list<RciOutpage>::const_iterator itData = _lastDataDisplayed.begin();
       itData != _lastDataDisplayed.end(); ++itData)
  {
    makeMessageDisplay( itData->getText(), field, value );
    makeMessageAction( itData->getAction(), field, action );

    boost::asio::write( inDevice->_socket, boost::asio::buffer(value) );
    boost::asio::write( inDevice->_socket, boost::asio::buffer(action) );
    
    ssMessage.str("");
    ssMessage << "Display: " << itData->getText() << std::endl;
    ssMessage << "Action: " << itData->getAction();
    Rci::RciDeviceBase::_logger.logIt( ssMessage, LgCpp::Logger::logINFO );
    
    ++field;
  }
  
//  Rci::RciDeviceBase::_logger.logIt( "updating device", LgCpp::Logger::logINFO );
}

/** asynchronously read data from the given device
 *  @param[in,out] inDevice: device to read
 */
void Rci::PhoneDevice::read( BluetoothConnection_ptr inDevice)
{
  switch( inDevice->_currentReadingStep )
  {
    case PhoneConnection::ReadingPacketSize:
    {
      inDevice->_data.resize(sizeof(uint32_t), 0x00);
      boost::asio::async_read(inDevice->_socket, boost::asio::buffer(inDevice->_data), 
            boost::bind( &PhoneDevice::readHandler, this, inDevice, _1, _2 ) );
      break;
    }
    case PhoneConnection::ReadingData:
    {
      std:size_t dataSize = inDevice->_currentPacketSize;
      inDevice->_data.resize(dataSize - sizeof(uint32_t), 0x00);
      boost::asio::async_read(inDevice->_socket, boost::asio::buffer(inDevice->_data), 
            boost::bind( &PhoneDevice::readHandler, this, inDevice, _1, _2 ) );
      break;
    }
    default:
    {
      break;
    }
  }
}

/** asynchronously write data to the given device
 *  @param[in,out] inDevice: device to write
 *  @note writeHandler must be called when written
 */
void Rci::PhoneDevice::writeFunction( BluetoothConnection_ptr inDevice,  Buffer_ptr inBuffer)
{
  boost::asio::async_write( inDevice->_socket, *inBuffer,
    boost::bind( &PhoneDevice::writeHandler, this, inDevice, inBuffer, _1, _2 ) );
}

void Rci::PhoneDevice::onReceive( BluetoothConnection_ptr inDevice, std::size_t inSize )
{
  std::ostringstream ssMessage;
  //checks if there are enough data.
  switch( inDevice->_currentReadingStep )
  {
    case PhoneConnection::ReadingPacketSize:
    {
      inDevice->_currentReadingStep = PhoneConnection::ReadingData;
      inDevice->_currentPacketSize = getInt(inDevice->_data);

      ssMessage << "waiting for a packet of size " << inDevice->_currentPacketSize;
      break;
    }
    case PhoneConnection::ReadingData:
      inDevice->_currentReadingStep = PhoneConnection::ReadingPacketSize;
      ssMessage << "packet received";
      handleData( inDevice );
      break;
    default:
      break;
  }
  Rci::RciDeviceBase::_logger.logIt( ssMessage, LgCpp::Logger::logINFO );
}

void Rci::PhoneDevice::onWritten( BluetoothConnection_ptr inDevice, std::size_t nChar){}

void Rci::PhoneDevice::handleData( BluetoothConnection_ptr inDevice )
{
  switch( (DeviceFunctions)inDevice->_data[0] )
  {
    case COMMAND:
    {
      inDevice->_nArguments = getInt( inDevice->_data, 1 );
      std::string code( (char*)&(inDevice->_data[5]), inDevice->_data.size() - 5 - 1 );
      inDevice->_receivedCode.setCode( code );
      inDevice->_receivedCode.clearArguments();
      break;
    }
    case ARGUMENTS:
    {
      // argument index, useless?
      // uint32_t index = getInt( inDevice->_data );
      // number of arguments should not change
      // uint32_t nArguments = getInt( inDevice->_data, 4 );
      std::string argument( (char*)&(inDevice->_data[9]), inDevice->_data.size() - 9 - 1 );
      inDevice->_receivedCode.addArgument( argument );
      break;
    }
  }
  
  if( inDevice->codeReady() )
  {
    RcCode code;
    RcCode::swap(code, inDevice->_receivedCode);
    code.setDeviceSpecificData(new PhoneSpecificData(this, inDevice));
    code.setTranslatorName( "PhoneDevice" );
    code.setIsNew( true );
    code.setNumberOfConsecutive( 1 );
    sendCommand(code);
  }
}

/** sends the data to print to the device
 *  @param[in] inDataToDisplay : data to send to the device
 *  @param[in] inOutpageName : page to display
 */
int Rci::PhoneDevice::echoPage( const std::list<RciOutpage> &inDataToDisplay,
                                const std::string &inOutpageName )
{
  _lastDataDisplayed = inDataToDisplay;

  if( _acceptedConnectionsSet.size() == 0 )
    return 0;

  bool force = false;
  if( _lastRequestedPage != inOutpageName )
  {
    force = true;
    _lastRequestedPage = inOutpageName;
    clearDevices();
  }

  unsigned char field = 0;
  _mDeviceList.lock();
    MessageData value, action;
    for( std::list<RciOutpage>::const_iterator itData = _lastDataDisplayed.begin();
         itData != _lastDataDisplayed.end(); ++itData)
    {
      if( itData->constGetChanged() || force )
      {
        makeMessageDisplay( itData->getText(), field, value );
        makeMessageAction( itData->getAction(), field, action );

        for( std::set<BluetoothConnection_ptr>::iterator itConn = _acceptedConnectionsSet.begin();
             itConn != _acceptedConnectionsSet.end(); ++itConn )
        {
          Buffer_ptr bufferValue(new boost::asio::const_buffers_1(boost::asio::buffer(value)));
          Buffer_ptr bufferAction(new boost::asio::const_buffers_1(boost::asio::buffer(action)));         
          write( *itConn, bufferValue );
          write( *itConn, bufferAction );
        }
      }
      ++field;
    }
  _mDeviceList.unlock();
}

/** clear devices 
 */
void Rci::PhoneDevice::clearDevices()
{
/*
  int msgSize = 0;
  char* msg = makeMessageClear( msgSize );
  writeDevices( _devicesList, msg, msgSize );
  delete [] msg;
  msg = NULL;
*/
}

int Rci::PhoneDevice::translate( RcCode &outCmd)
{
    
  int ans = -1;

  if( !_codesQueue.empty() )
  {
    outCmd.setDeviceSpecificData(NULL);
    RcCode::swap( _codesQueue.front(), outCmd );
    _codesQueue.pop();
    outCmd.setTranslatorName( "PhoneDevice" );
    outCmd.setIsNew( true );
    outCmd.setNumberOfConsecutive( 1 );
    ans = 0;
  }
  return ans;
}

void Rci::PhoneDevice::setAnswer( std::string const& inBackAction, CmdAnswer const& inAnswer,
                                  DeviceSpecificData *inSpecificData)
{  
  if( inSpecificData->getRemoteDevice() == this && inAnswer.size() > 0 )
  {    
    MessageData value;
    makeMessageAnswerAction( inBackAction, value );
    Buffer_ptr bufferAction(new boost::asio::const_buffers_1(boost::asio::buffer(value)));         
    write( ((PhoneSpecificData*)inSpecificData)->getPhoneConnection(), bufferAction );
    
    
    for( int n = 0; n < inAnswer.size(); ++n )
    {
      // send the associated action or ...
      
      makeMessageAnswer( inAnswer[n], n, inAnswer.size(), value );

      Buffer_ptr bufferValue(new boost::asio::const_buffers_1(boost::asio::buffer(value)));         
      write( ((PhoneSpecificData*)inSpecificData)->getPhoneConnection(), bufferValue );
    }
  }
}
