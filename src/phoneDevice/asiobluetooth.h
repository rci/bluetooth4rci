#ifndef _ASIOBLUETOOTH_H_
#define _ASIOBLUETOOTH_H_

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

extern "C" {
  #include "myBluetooth.h"
}

#include <set>
#include <sstream>
#include <logcplusplus.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

namespace abt
{
class BluetoothProtocol
{
/// Encapsulates the flags needed for bluetooth sockets.
/**
 * The abt::bluetoothProtocol class contains flags necessary for
 * bluetooth sockets.
 *
 * @par Thread Safety
 * @e Distinct @e objects: Safe.@n
 * @e Shared @e objects: Safe.
 *
 * @par Concepts:
 * Protocol.
 */

public:
  /// Obtain an identifier for the type of the protocol.
  int type() const
  {
    return SOCK_STREAM;
  }

  /// Obtain an identifier for the protocol.
  int protocol() const
  {
    return BTPROTO_RFCOMM;
  }

  /// Obtain an identifier for the protocol family.
  int family() const
  {
    return PF_BLUETOOTH;
  }
  
  /// The type of a bluetooth endpoint.
  typedef boost::asio::local::basic_endpoint<BluetoothProtocol> endpoint;

  /// The bluetooth socket type.
  typedef boost::asio::basic_stream_socket<BluetoothProtocol> socket;

  /// The bluetooth acceptor type.
  typedef boost::asio::basic_socket_acceptor<BluetoothProtocol> acceptor;

};

class BluetoothConnection
{
public:
  BluetoothConnection( boost::asio::io_service& inService ): _socket(inService),
                      _logger("BluetoothConnection"){}

  ~BluetoothConnection(){}
  
  BluetoothProtocol::socket _socket;
  LgCpp::Logger _logger;
  std::vector<uint8_t> _data;
};

template<class TemplateConnection>
class BluetoothServer
{
public:
  typedef boost::shared_ptr<TemplateConnection> Connection_ptr;
  typedef boost::shared_ptr<boost::asio::const_buffers_1 > Buffer_ptr;
private:
  boost::asio::io_service _asioService;
  BluetoothProtocol::endpoint _endpoint;
  BluetoothProtocol::acceptor _acceptor;
  
  boost::thread *_asioServiceThread;

  uuid_t _serviceUuid;
  sdp_session_t *_serviceSession;

  LgCpp::Logger _logger;

protected:
  Connection_ptr _pendingConnection;
  std::set<Connection_ptr> _acceptedConnectionsSet;
  std::set<Buffer_ptr> _bufferSet;
  boost::mutex _mDeviceList;
  boost::mutex _mBufferSet;

private:

  void asioServiceThread() { _asioService.run(); } ///< runs the io service  
  /** close the sdp session if already opened
   */
  void closeSdpSession()
  {
    if( _serviceSession != NULL )
    {
      sdp_close( _serviceSession );
      _serviceSession = NULL;
    }
  }
  
  /** bind the server to a bluetooth device and set the acceptor in the listening mode
   */
  void listen()
  {    
    bool connected = false;
    struct sockaddr_rc* loc_addr;
    loc_addr = (sockaddr_rc*)_endpoint.data();

    int channel;
    for( int tstChannel = 2; (tstChannel <= 30) && !connected; ++tstChannel )
    {
      loc_addr->rc_channel = tstChannel;
      
      try
      {
        _acceptor.bind( _endpoint );
        connected = true;
        channel = tstChannel;
      } catch ( boost::system::system_error e ) { connected = false; }
    }
    
    if( !connected )
    {
      ///@todo handle error
      _logger.logIt("connection failed at binding", LgCpp::Logger::logINFO);
    }
    else
    {
      std::ostringstream ssMessage;
      ssMessage << "binded to channel " << channel << " on socket " << _acceptor.native();
      _logger.logIt( ssMessage, LgCpp::Logger::logINFO );

      try
      {
        _acceptor.listen();
      } catch ( boost::system::system_error e )
      {
        ///@todo handle error
        _logger.logIt("connection failed at listening", LgCpp::Logger::logINFO);      
      }
    }
  }
  
  /// accept connections
  void accept()
  {
    Connection_ptr newConnection( new TemplateConnection(_asioService) );
    _pendingConnection = newConnection;
    _acceptor.async_accept( _pendingConnection->_socket,
                            boost::bind( &BluetoothServer::acceptHandler, this, _pendingConnection, _1 ) );
  }
  
  /** Called when a new connection has been accepted by asio, or an error occured during accepting process
   *  @param[in] newConnection: accepted connection.
   *  @param[in] ec: error code
   */
  void acceptHandler( Connection_ptr newConnection,
                                       const boost::system::error_code& ec )
  {
    if(!ec)
    {
      std::ostringstream ssMessage;
      ssMessage << "connection accepted on socket " << newConnection->_socket.native();
      _logger.logIt( ssMessage, LgCpp::Logger::logINFO );
      
      _mDeviceList.lock();
        _acceptedConnectionsSet.insert(newConnection);
      _mDeviceList.unlock();
      
      onConnection(newConnection);
      read(newConnection);
    }
    else
    {
      std::ostringstream ssMessage;
      ssMessage << ec.message();
      _logger.logIt( ssMessage, LgCpp::Logger::logWARNING );
    }
    if(ec != boost::asio::error::operation_aborted)
      accept();
  }
  
protected:
  /** Called when data have been received on the connection, or an error occured during reading process
   *  @param[in] newConnection: accepted connection.
   *  @param[in] ec: error code
   *  @param[in] nChar: number of data received
   */
  void readHandler( Connection_ptr inConnection,
                                       const boost::system::error_code& ec, std::size_t nChar )
  {
    std::ostringstream ssMessage;
    if(!ec)
    {
      onReceive( inConnection, nChar );
      read(inConnection);
    }
    else
    {
      ssMessage << "reading stopped with error " << ec;
      _logger.logIt( ssMessage, LgCpp::Logger::logWARNING );
      onDisconnect(inConnection);
      _mDeviceList.lock();
        _acceptedConnectionsSet.erase(inConnection);
      _mDeviceList.unlock();
    }
  }
    
  /** Called when data have been written on the connection, or an error occured during reading process
   *  @param[in] newConnection: accepted connection.
   *  @param[in] ec: error code
   *  @param[in] nChar: number of data received
   */
  void writeHandler( Connection_ptr inConnection, Buffer_ptr inBuffer,
                     const boost::system::error_code& ec, std::size_t nChar )
  {
    if(!ec)
      onWritten( inConnection, nChar );
    _mBufferSet.lock();
      _bufferSet.erase(inBuffer);
    _mBufferSet.unlock();
  }

  void write( Connection_ptr inConnection, Buffer_ptr inBuffer )
  {
    _mBufferSet.lock();
      _bufferSet.insert(inBuffer);
    _mBufferSet.unlock();
    writeFunction(inConnection, inBuffer);
  }
  
  virtual void read( Connection_ptr ) = 0;
  virtual void writeFunction( Connection_ptr, Buffer_ptr ) = 0;
  virtual void onConnection( Connection_ptr ) = 0;
  virtual void onDisconnect( Connection_ptr ) = 0;
  virtual void onReceive( Connection_ptr, std::size_t ) = 0;
  virtual void onWritten( Connection_ptr, std::size_t ) = 0;
  
public:
  BluetoothServer() : _acceptor( _asioService ),
    _asioServiceThread(NULL), _logger("BluetoothServer"), _serviceSession(NULL)
  {
    _logger.logIt("creating server", LgCpp::Logger::logINFO);

    _acceptor.open( BluetoothProtocol() );
    
    struct sockaddr_rc* loc_addr;
    _endpoint.resize( sizeof(sockaddr_rc) );
    loc_addr = (sockaddr_rc*)_endpoint.data();
    
  	makeAddressRFCOMM(loc_addr);
  	
  	listen();
  	
  	accept();
  	
  	_asioServiceThread = new boost::thread( boost::bind( &BluetoothServer::asioServiceThread, this) );
  }
    
  ~BluetoothServer()
  {
    _acceptor.cancel();
    _asioService.stop();
    _asioServiceThread->join();
    delete _asioServiceThread;
    
    closeSdpSession();
  }

  /** Advertise the service with the given uuid on the opened channel.
   *  @param[in] inServiceUuid: uuid of the service to advertise.
   *  @param[in] serviceName: human readable name of the service.
   *  @param[in] serviceDescription: human readable description of the service.
   *  @param[in] serviceProvider: human readable provider of the service.
   *  @return -1 on error.
   *  @todo handle the case when the bluetooth port has not been opened.
   */
  int advertise( uuid_t inServiceUuid, const std::string& serviceName,
                  const std::string& serviceDescription, const std::string& serviceProvider )
  {
    int ans = 0;

    uint8_t channel = ((sockaddr_rc*)_endpoint.data())->rc_channel;
    
    // close the sdp session if already opened
    closeSdpSession();
    
    _serviceSession = registerService( channel, inServiceUuid, serviceName.c_str(),
                                serviceDescription.c_str(), serviceProvider.c_str() );
    
    if( _serviceSession == NULL )
      ans = -1;
    return ans;
  }
};
}

#endif
