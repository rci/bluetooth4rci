#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include "BTMessageHandler.h"

const unsigned int Rci::BTMessageHandler::_MAX_PACKET_SIZE = 1024;
const unsigned int Rci::BTMessageHandler::_MAX_HEADER_SIZE = 16;

Rci::BTMessageHandler::BTMessageHandler(): _messageId(0), _msgSourceId(0), _messageSize(0),
      _isReading(false), _readingThread(NULL)
{
  BTPacketData firstPacket;
  _message.push_back(firstPacket);
}

Rci::BTMessageHandler::~BTMessageHandler() {}

/** Adds data to the header
 *  @param[in] inData : data to add
 *  @param[in] inSize : size of the data to add
 *  @return size of the data added
 **/
unsigned int Rci::BTMessageHandler::addToHeader( void* inData, unsigned int inSize)
{
  unsigned int remainingSpace = _MAX_HEADER_SIZE - _rawHeader.size();
  unsigned int sizeOfDataToWrite = ( remainingSpace > inSize ? inSize : remainingSpace );
  if(sizeOfDataToWrite != 0)
    _rawHeader.insert(_rawHeader.end(), (uint8_t*)inData, (uint8_t*)(inData + sizeOfDataToWrite));
  return sizeOfDataToWrite;
}

/** Adds data to a given packet
 *  @param[in,out] inOutPacket : packet to fill
 *  @param[in] inData : data to add
 *  @param[in] inSize : size of the data to add
 *  @return size of the data added
 **/
unsigned int Rci::BTMessageHandler::addToPacket( BTPacketData &inOutPacket, void* inData, unsigned int inSize)
{
  unsigned int remainingSpace =  _MAX_PACKET_SIZE - inOutPacket.size();
  unsigned int sizeOfDataToWrite = ( remainingSpace > inSize ? inSize : remainingSpace );
  if(sizeOfDataToWrite != 0)
    inOutPacket.insert(inOutPacket.end(), (uint8_t*)inData, (uint8_t*)(inData + sizeOfDataToWrite));
  return sizeOfDataToWrite;
}

/** Returns the message built with the packets in memory
 *  @return message built with the packets in memory
 */
const Rci::BTMessageHandler::BTMessage& Rci::BTMessageHandler::getMessage()
{
  BTMessage message;
  message.reserve(_message.size() * _MAX_PACKET_SIZE);
  std::list<BTPacketData>::iterator itPacket;
  for( itPacket = _message.begin(); itPacket != _message.end(); ++itPacket)
    message.insert(message.end(), itPacket->begin(), itPacket->end());
  return message;
}

/** Checks the header of the incomming message. If complete, extracts the data and compare them to the last
 *  message received. If the message ID is the same, do nothing. Else, drop the last message and write a new
 *  one.
 *  @return true if the new header is ready
 */
bool Rci::BTMessageHandler::checkHeader()
{
  bool ans = false;
  if( _rawHeader.size() == _MAX_HEADER_SIZE )
  {
    ans = true;
    // read the header
    uint32_t newMessageId   = *(uint32_t*)(&_rawHeader[0]);
    uint32_t newMsgSourceId = *(uint32_t*)(&_rawHeader[4]);
    uint32_t newMsgSize     = *(uint32_t*)(&_rawHeader[8]);
    uint32_t newPacketSize  = *(uint32_t*)(&_rawHeader[12]);  // end of the header
    
    // check the data
    if( newMessageId != _messageId )
    {
      // new message received drop the old one
      _message.clear();
      _currentMessageSize = 0;
    }
    else
    {
      if( newMsgSourceId != _msgSourceId )
      {
        ///@todo handle warning
      }
      if( newMsgSize != _messageSize )
      {
        ///@todo handle error
      }
    }
    
    _messageId = newMessageId;
    _msgSourceId = newMsgSourceId;
    _messageSize = newMsgSize;
    _currentPacketSize = newPacketSize;
    
    _rawHeader.clear();
  }
  return ans;
}

/** Adds data to a message
 *  @param[in] inData : data to add
 *  @param[in] inSize : size of the data to add
 **/
void Rci::BTMessageHandler::addData( void* inData, unsigned int inSize)
{ 
  unsigned int dataWritten = 0;
  while( dataWritten != inSize )
  {
    dataWritten += addToPacket( _message.back(), inData + dataWritten, inSize - dataWritten );
    if( dataWritten != inSize)
      _message.push_back( BTPacketData() );
  }
}

void Rci::BTMessageHandler::socketReader( int inSocket )
{
  if( inSocket > 0 )
  {
    char buffer[_MAX_PACKET_SIZE];
    bool stopReading = false;
    unsigned int dataSize = read( inSocket, buffer, _MAX_PACKET_SIZE );
    if( dataSize > 0 )
    {
      unsigned int position = 0;
      while( position < dataSize )
      {
        // read header if a new packet is incomming
        if(_message.back().size() == 0)
          position += addToHeader( buffer, dataSize );
        // compare header to the current message readed if present
        checkHeader();
        // complete packet with data
        unsigned int remainingSpace = _currentPacketSize - _message.back().size();
        unsigned int dataToAdd = (remainingSpace > dataSize - position )? dataSize - position : remainingSpace ;
        addToPacket( _message.back(), buffer + position, dataToAdd);
        position += dataToAdd;
        _currentMessageSize += dataToAdd;
        // check if the packet is full
        if( _currentPacketSize == _message.back().size() )
          // add an empty packet
          _message.push_back( BTPacketData() );
        // if message is complete, trig action
        if( _currentMessageSize == _messageSize )
          messageCallback( getMessage() );
      }
    }
    else if( dataSize == -1 )
    {
    }
  }
}

void startReading( int inSocket )
{

}



