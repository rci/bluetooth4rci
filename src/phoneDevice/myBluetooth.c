#include "myBluetooth.h"

/** finds a channel for the given bluetooth device and service on the given device
 *  @param inDeviceAddress : device to check
 *  @param inSvcUuid : UUID of the service to find out
 *  @return corresponding channel, -1 on error
 */
int getChannelFromUUID( bdaddr_t inDeviceAddress , uuid_t inSvcUuid )
{
  int err;
  int channel = -1;
  // connect to the SDP server running on the remote machine
  sdp_session_t *session = NULL;

  session = sdp_connect( BDADDR_ANY, &inDeviceAddress, SDP_RETRY_IF_BUSY );
  
  if( session != NULL )
  {
    sdp_list_t *response_list = NULL, *search_list, *attrid_list;
    search_list = sdp_list_append( NULL, &inSvcUuid ); // list of uuid to search for
    
    // specify that we want a list of all the matching applications' attributes
    uint32_t range = 0x0000ffff;
    attrid_list = sdp_list_append( NULL, &range );
    
    // get a list of service records that have UUID 0xabcd
    err = sdp_service_search_attr_req( session, search_list, SDP_ATTR_REQ_RANGE, attrid_list, &response_list);


    sdp_list_t *r = response_list;
    // go through each of the service records
    for (; r; r = r->next ) {
      sdp_record_t *rec = (sdp_record_t*) r->data;
      sdp_list_t *proto_list;
      
      // get a list of the protocol sequences
      if( sdp_get_access_protos( rec, &proto_list ) == 0 ) {
      sdp_list_t *p = proto_list;

      // go through each protocol sequence
      for( ; p ; p = p->next ) {
        sdp_list_t *pds = (sdp_list_t*)p->data;

        // go through each protocol list of the protocol sequence
        for( ; pds ; pds = pds->next ) {

          // check the protocol attributes
          sdp_data_t *d = (sdp_data_t*)pds->data;
          int proto = 0;
          for( ; d; d = d->next ) {
            switch( d->dtd ) { 
              case SDP_UUID16:
              case SDP_UUID32:
              case SDP_UUID128:
                  proto = sdp_uuid_to_proto( &d->val.uuid );
                  break;
              case SDP_UINT8:
                  if( proto == RFCOMM_UUID )
                      channel = d->val.int8;
                  break;
            }
          }
        }
        sdp_list_free( (sdp_list_t*)p->data, 0 );
      }
      sdp_list_free( proto_list, 0 );

      }
      sdp_record_free( rec );
    }
    sdp_list_free( response_list, 0 );
    sdp_list_free( search_list, 0 );
    sdp_list_free( attrid_list, 0 );
    sdp_close(session);
  }

  return channel;
}

int getChannelFromUUID_c( const char* inDeviceAddress , uuid_t inSvcUuid )
{
  bdaddr_t deviceAddress;
  str2ba( inDeviceAddress, &deviceAddress );
  return getChannelFromUUID( deviceAddress, inSvcUuid );
}

/** connects to the device on the given RFCOMM channel
 *  @param inDeviceAddress : device to connect
 *  @param inChannel : channel to connect
 *  @return file descriptor of the connection, -1 on error
 */
int connectDeviceRFCOMM( bdaddr_t inDeviceAddress , uint8_t inChannel )
{
  struct sockaddr_rc address = { 0 };
  int devSocket, status;

  int ans;

  // allocate a socket
  devSocket = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
  if( devSocket >= 0 )
  {
    // set the connection parameters (who to connect to)
    address.rc_family = AF_BLUETOOTH;
    address.rc_channel = (uint8_t) inChannel;
    address.rc_bdaddr = inDeviceAddress;

    // connect to server
    status = connect( devSocket, (struct sockaddr *)&address, sizeof(address) );

    int flags = fcntl(devSocket, F_GETFL, 0);
    fcntl(devSocket, F_SETFL, flags | O_NONBLOCK);

    if( status != 0 && errno != EAGAIN )
    {
      close(devSocket);
      ans = -1;
    }
    else
      ans = devSocket;
  }
  else
    ans = -1;
  
  return ans;
}

int connectDeviceRFCOMM_c( const char* inDeviceAddress, uint8_t inChannel )
{
  bdaddr_t deviceAddress;
  str2ba( inDeviceAddress, &deviceAddress );
  return connectDeviceRFCOMM( deviceAddress, inChannel );
}

/** fills the parameters of the given socket address
 *  @param[in,out] inOutAddress : channel to connect to
 *  @return -1 on error
 */
int makeAddressRFCOMM( struct sockaddr_rc *inOutAddress )
{
  int ans = -1;
  if(inOutAddress != NULL)
  {
    ans = 0;
    
    // clears all data contained in the inputed address
    memset(inOutAddress, 0, sizeof(struct sockaddr_rc));

    inOutAddress->rc_family = AF_BLUETOOTH;
    inOutAddress->rc_bdaddr = *BDADDR_ANY;
    inOutAddress->rc_channel = 0;
  }
  return ans;
}

/** opens an RFCOMM channel
 *  @param[in,out] inChannel : channel to connect to
 *  @return socket of the server, -1 on error
 */
int makeServerRFCOMM( uint8_t *inoutChannel )
{
  int ans = 0;
  
  // allocate socket
  int s = socket(PF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
  if( s >= 0 )
  {
    // bind socket to port 1 of the first available 
    // local bluetooth adapter
    struct sockaddr_rc loc_addr = { 0 };
  	memset(&loc_addr, 0, sizeof(loc_addr));

    loc_addr.rc_family = AF_BLUETOOTH;
    loc_addr.rc_bdaddr = *BDADDR_ANY;
//    bacpy(&loc_addr.rc_bdaddr, BDADDR_ANY);
  

    // try to bind to the requested channel. If inoutChannel == 0
    // tries to connect to every channel.
    uint8_t maxChannel;
    if( *inoutChannel == 0 )  
    {
      maxChannel = 30;
      *inoutChannel = 2;
    }
    else
      maxChannel = *inoutChannel;
    
    uint8_t i;
    int ok = -1;
    for ( i = *inoutChannel; ( i <= maxChannel ) && ( ok < 0 ); ++i )
    {
      loc_addr.rc_channel = i;
      int size;
      ok = bind(s, (struct sockaddr *)&loc_addr, sizeof( loc_addr ));
      printf("bind socket %d, result %d, size %d, channel %d\n", s, ok, sizeof( loc_addr ), loc_addr.rc_channel );
      if( ok == 0 )
      {
        *inoutChannel = i;

        int opt, size;
        printf("getsockopt result %d, ", getsockopt(s, SOL_RFCOMM, RFCOMM_LM, &opt, &size));
        printf("opt %d\n", opt);
        printf("new opt %d\n", opt);
        printf("setsockopt result %d\n", setsockopt(s, SOL_RFCOMM, RFCOMM_LM, &opt, sizeof(opt)));
      }
    }
    
    ans = ok;
    
    if( ans >= 0 )
      // put socket into listening mode
      ans = listen(s, 10);

    // puts the socket in non blocking mode
    int flags = fcntl(s, F_GETFL, 0);
    fcntl(s, F_SETFL, flags | O_NONBLOCK);

    if( ans < 0 )
      close( s );
    else
      ans = s;
  }
  else
    ans = -1;
  return ans;
}

/** wait for a client to connect on the given socket
 *  @param[in] s : socket to wait for
 *  @return client, -1 on error
 */
int acceptClientRFCOMM( int s, char* outAddr)
{
  int clientSocket;
  struct sockaddr_rc rem_addr = { 0 };
  socklen_t opt = sizeof(rem_addr);

  // accept one connection
  clientSocket = accept(s, (struct sockaddr *)&rem_addr, &opt);

  if( s > 0 )
  {
    bdaddr_t test = rem_addr.rc_bdaddr;

    ba2str( &test , outAddr );
  
    // puts the socket in non blocking mode
    int flags = fcntl(clientSocket, F_GETFL, 0);
    fcntl(clientSocket, F_SETFL, flags | O_NONBLOCK);
  }
  return clientSocket;
}

/** advertise the given RFCOMM service
 *  @param[in] inChannel : RFCOMM channel of the service
 *  @param[in] inSvcUuid : service UUID
 *  @param[in] svcName : service name
 *  @param[in] svcDescription : service description
 *  @param[in] svcProvider : service provider
 *  @return pointer to the sdp session, NULL if error.
 */
sdp_session_t *registerService( uint8_t inChannel, uuid_t inSvcUuid, const char* svcName,
                                const char* svcDescription, const char* svcProvider )
{
  sdp_list_t *svc_class_list = NULL, *root_list = NULL, *rfcomm_list = NULL, *proto_list = NULL,
             *profile_list = NULL, *access_proto_list = NULL, *l2cap_list = NULL;

  // set the general service ID
  sdp_record_t record = { 0 };
  uuid_t svc_class_uuid, root_uuid, rfcomm_uuid, l2cap_uuid;

  // set the service class
  svc_class_list = sdp_list_append(0, &inSvcUuid);
  sdp_set_service_classes(&record, svc_class_list);

  // set the Bluetooth profile information
  sdp_profile_desc_t profile;
  sdp_uuid16_create(&profile.uuid, SERIAL_PORT_PROFILE_ID);
  profile.version = 0x0100;
  profile_list = sdp_list_append(0, &profile);

  // make the service record publicly browsable
  sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
  root_list = sdp_list_append(0, &root_uuid);
  sdp_set_browse_groups( &record, root_list );

  // set rfcomm information
  sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
  sdp_data_t *channel = sdp_data_alloc(SDP_UINT8, &inChannel);
  rfcomm_list = sdp_list_append( 0, &rfcomm_uuid );
  sdp_list_append( rfcomm_list, channel );
  proto_list = sdp_list_append( 0, rfcomm_list );
  
  // set l2cap information
  ///< we says that we support l2cap even if it's not true. Some phones won't discover the service otherwise.
  sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
  l2cap_list = sdp_list_append( 0, &l2cap_uuid );
  sdp_list_append( proto_list, l2cap_list );
  access_proto_list = sdp_list_append( 0, proto_list );
  sdp_set_access_protos( &record, access_proto_list );
  
  // set the name, provider, and description
  sdp_set_info_attr(&record, svcName, svcProvider, svcDescription);
  
  int err = 0;
  sdp_session_t *session = NULL;  
  // connect to the local SDP server, register the service record, and 
  // disconnect
  session = sdp_connect( BDADDR_ANY, BDADDR_LOCAL, SDP_RETRY_IF_BUSY );
  err = sdp_record_register(session, &record, 0);
  
  // cleanup
  sdp_data_free( channel );
  sdp_list_free( svc_class_list, 0 );
  sdp_list_free( profile_list, 0 );
  sdp_list_free( rfcomm_list, 0 );
  sdp_list_free( l2cap_list, 0 );
  sdp_list_free( root_list, 0 );
  sdp_list_free( proto_list, 0 );
  sdp_list_free( access_proto_list, 0 );

//  sdp_record_free( &record );

  return session;
}

