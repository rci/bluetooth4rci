/** @file myBluetooth.h
 *  @note code inspired or obtained from http://people.csail.mit.edu/albert/bluez-intro
 */


#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#ifndef _MYBLUETOOTH_H_
#define _MYBLUETOOTH_H_

/// finds a channel for the given bluetooth device and service on the given device
int getChannelFromUUID( bdaddr_t inDeviceAddress , uuid_t inSvcUuid );
int getChannelFromUUID_c( const char* inDeviceAddress , uuid_t inSvcUuid );

/// connects to a device using RFCOMM
int connectDeviceRFCOMM( bdaddr_t inDeviceAddress, uint8_t inChannel );
int connectDeviceRFCOMM_c( const char* inDeviceAddress, uint8_t inChannel );

/// registering an RFCOMM service
sdp_session_t *registerService( uint8_t inChannel, uuid_t inSvcUuid, const char* svcName,
                                const char* svcDescription, const char* svcProvider );

/// make RFCOMM socket address
int makeAddressRFCOMM( struct sockaddr_rc *inOutAddress );

/// make RFCOMM server
int makeServerRFCOMM( uint8_t *inoutChannel );

/// wait for client connection on the given socket
int acceptClientRFCOMM( int s, char* outAddr );

#endif
