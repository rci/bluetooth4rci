#ifndef _BTMESSAGE_H_
#define _BTMESSAGE_H_

#include <stdint.h>
#include <list>
#include <vector>
#include <boost/thread.hpp>

namespace Rci
{
class BTMessageHandler
{
  typedef std::vector<uint8_t> BTHeader;
  typedef std::vector<uint8_t> BTPacketData;
  typedef std::vector<uint8_t> BTPacketHeader;
  typedef std::vector<uint8_t> BTMessage;
  
private:
  static const unsigned int _MAX_PACKET_SIZE;
  static const unsigned int _MAX_HEADER_SIZE;
  
  uint32_t _messageId;
  uint32_t _msgSourceId;
  uint32_t _messageSize;
  uint32_t _currentMessageSize;
  uint32_t _currentPacketSize;
  BTHeader _rawHeader;
  std::list<BTPacketData> _message;
  boost::thread* _readingThread;
  
  int _socketToRead;
  bool _isReading;
  
protected:
  void setMessageId( uint32_t inId ){_messageId = inId;}
  void setMsgSourceId( uint32_t inId ){_msgSourceId = inId;}
  
public:
  uint32_t getMessageId(){return _messageId;} 
  uint32_t getMsgSourceId(){return _msgSourceId;}

private:
  unsigned int addToHeader( void*, unsigned int );
  unsigned int addToPacket( BTPacketData &, void*, unsigned int );
  const BTMessage& getMessage();
  
  bool checkHeader();
  
  void socketReader( int );

public:
  BTMessageHandler();
  ~BTMessageHandler();
  
  void startReading( int );
  void stopReading(){ }
  void writeDevice( int );
  void addData( void*, unsigned int ); ///< add any type of serialisable data to the message
  virtual void messageCallback( const BTMessage& ) = 0; ///< called when a message is received
};
}

#endif
