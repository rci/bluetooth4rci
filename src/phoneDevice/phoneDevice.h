#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <boost/thread.hpp>
#include <rciDaemon/rciDeviceBase.h>

#include "asiobluetooth.h"
#include "../fileDevice/fileDevice.h"


#ifndef _PHONEDEVICE_H_
#define _PHONEDEVICE_H_

namespace Rci
{
typedef std::vector<char> MessageData;
typedef uint32_t PacketSize;

class PhoneConnection :
  public abt::BluetoothConnection
{
  public:
    enum ReadingStep
    {
      ReadingPacketSize,
      ReadingData,
      Finish
    };
    
    int _nArguments;
    RcCode _receivedCode;
    
    PhoneConnection(boost::asio::io_service& inIoService) : abt::BluetoothConnection(inIoService),
          _currentReadingStep(ReadingPacketSize), _currentPacketSize(0) {}
    ReadingStep _currentReadingStep;    ///< current step of the reading
    PacketSize _currentPacketSize;  ///< current packet size 
    
    bool codeReady(){ return _nArguments == _receivedCode.getArgumentsNumber();}
};

/** This class defines the screen sizes
 */
class PhoneDevice :
  public FileDevice,
  virtual public abt::BluetoothServer<PhoneConnection>
{
public:
  typedef abt::BluetoothServer<PhoneConnection>::Connection_ptr BluetoothConnection_ptr;
protected:
  uint8_t _svc_uuid_int[32];
  
  enum Commands
  {
    Display = 1,  // data to display
    Answers = 2,  // answer to a request
    Action  = 3,  // action associated with a displayed data
    AnswerAction = 4, // action to perform with the answers sent
    Clear = 5     // clear the displayed data
  };

  enum DeviceFunctions
  {
    COMMAND = 1,
    ARGUMENTS = 2
  };

  class PhoneSpecificData : public DeviceSpecificData
  {
    private:
      BluetoothConnection_ptr _btConnection;
      LgCpp::Logger _logger;
    public:
      PhoneSpecificData( RciDeviceBase* inRemoteDevice, BluetoothConnection_ptr inConnection ) : 
            DeviceSpecificData(inRemoteDevice), _btConnection(inConnection), _logger("PhoneSpecificData") {}
      PhoneSpecificData( const PhoneSpecificData& other ) : DeviceSpecificData(other),
            _btConnection(other._btConnection), _logger("PhoneSpecificData")
      {
      }
      ~PhoneSpecificData()
      {
      }


      PhoneSpecificData& operator=(const PhoneSpecificData& other)
      {
        _btConnection = other._btConnection;
        return *this;
      }

      PhoneSpecificData* clone() const
      {
        return new PhoneSpecificData(*this);
      }

      BluetoothConnection_ptr getPhoneConnection(){ return _btConnection; }
  };
  

  ///list of last data displayed
  std::list<RciOutpage> _lastDataDisplayed;

  ///queue of the received commands
  std::queue<RcCode> _codesQueue;
  
  uint32_t getInt(const std::vector<uint8_t>& inData, int start = 0)
  {
    uint32_t value;
    for(int i = 0; i < sizeof(uint32_t); ++i)
    {
      value = ( value << 8 ) + (unsigned char)inData[i + start];
    }
    return value;
  }
  
  /// makes the Action message, used to send the action related to the displayed data
  void makeMessageAction( const std::string&, unsigned char, MessageData& );
  /// makes the Answer message, used to send the answers to actions triggered by the phone
  void makeMessageAnswer( const std::string&, unsigned char, unsigned char, MessageData& );
  /// makes the AnswerAction message, used te tell the phone what to do with the answers
  void makeMessageAnswerAction( const std::string&, MessageData& );
  /// makes the Display message, to display data
  void makeMessageDisplay( const std::string&, unsigned char, MessageData& );
  /// makes the Display message, to display data
  void makeMessageClear( MessageData& );

  void clearDevices();
  
  void read( BluetoothConnection_ptr );
  void writeFunction( BluetoothConnection_ptr, Buffer_ptr );
  void onConnection( BluetoothConnection_ptr );
  void onDisconnect( BluetoothConnection_ptr inConnection ) {}
  void onReceive( BluetoothConnection_ptr, std::size_t );
  void onWritten( BluetoothConnection_ptr, std::size_t );
  void handleData( BluetoothConnection_ptr );

public:
  PhoneDevice(const std::string &inPath);
  virtual ~PhoneDevice();
  
  virtual int init(const std::string &inConfigPath);
  
  virtual int echoPage(const std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName);
  virtual int commandScreen(ScreenCommands inCommand){return 0;}
  
  virtual void setAnswer( std::string const& inBackAction, CmdAnswer const& inAnswer, DeviceSpecificData *inSpecificData);
  virtual int translate( RcCode &outCmd); ///< returns a command
};

}

#endif
